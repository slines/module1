﻿using System;
using Module1;

namespace M1
{
    public class Module1
    {
        static void Main(string[] args)
        {
            Console.WriteLine("i'm working!");
        }

        public int[] SwapItems(int a, int b)
        {
            int[] arr = new int[2] { b, a };
            return arr;
        }

        public int GetMinimumValue(int[] input)
        {
            Array.Sort(input);
            return input[0];
        }
    }
}
